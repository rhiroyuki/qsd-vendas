class Product
  attr_reader :name, :id, :description
  API_URL = Rails.configuration.api['products_api'].freeze
  PRODUCTS_URL = 'products'.freeze

  def initialize(params)
    params.symbolize_keys!
    @id = params[:id]
    @name = params[:name]
    @description = params[:description]
  end

  def self.all
    data = JSON.parse RestClient.get("#{API_URL}/#{PRODUCTS_URL}")
    data['products'].map do |product|
      Product.new(product)
    end
  end

  def self.find(id)
    response = RestClient.get("#{API_URL}/#{PRODUCTS_URL}/#{id}")
    response = JSON.parse(response.body)
    new response['product']
  end

  def plans
    response = RestClient.get("#{API_URL}/#{PRODUCTS_URL}/#{@id}/plans")
    response = JSON.parse(response.body)
    response['plans'].map do |plan|
      Plan.new plan
    end
  end
end
