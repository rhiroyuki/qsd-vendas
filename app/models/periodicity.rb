class Periodicity
  attr_reader :id, :name, :price
  BASE_URL = Rails.configuration.api['products_api']
  PLANS_URL = '/plans'.freeze

  def initialize(params)
    params.symbolize_keys!
    @id = params[:price_id]
    @name = params[:periodicity]
    @price = params[:price]
  end
end
