class Plan
  attr_reader :id, :name, :description, :details

  BASE_URL = Rails.configuration.api['products_api']
  PLANS_URL = 'plans'.freeze

  def initialize(params)
    params.symbolize_keys!
    @id = params[:id]
    @name = params[:name]
    @description = params[:description]
    @details = params[:details]
  end

  def self.find(plan_id)
    response = JSON.parse RestClient.get("#{BASE_URL}/#{PLANS_URL}/#{plan_id}")
    new response['plan']
  end

  def periodicities
    response = JSON.parse RestClient.get("#{BASE_URL}/#{PLANS_URL}/#{@id}")
    response['plan']['plan_option'].map do |periodicity|
      Periodicity.new(periodicity)
    end
  end
end
