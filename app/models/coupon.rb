class Coupon
  COUPON_URL = Rails.configuration.api['coupons_api'].freeze
  ENDPOINT = '/burn'.freeze
  attr_reader :key, :value, :type
  def initialize(params)
    @key = params[:key]
    @type = params[:type]
    @value = params[:value]
  end

  def self.apply_coupon(coupon_key, order)
    begin
      response = RestClient.patch("#{COUPON_URL}/#{coupon_key}#{ENDPOINT}",
                                  order_number: order.id,
                                  product_id: order.product)
    rescue
      return nil
    end
    return nil unless response.code == 200
    value = JSON.parse(response.body)['discount']
    new key: coupon_key, type: 'percent', value: value.to_f
  end
end
