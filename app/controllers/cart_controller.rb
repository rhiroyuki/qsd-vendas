class CartController < ApplicationController
  before_action :set_menus
  before_action :redirect_admin

  def plans
    @product = Product.find(params[:product_id])
    @plans = @product.plans
  end

  def prices
    @product = Product.find params[:product_id]
    @plan = Plan.find params[:plan_id]
    @periodicities = @plan.periodicities
  end

  def products
    @products = Product.all
  end
end
