class SellersController < ApplicationController
  before_action :set_menus
  def new
    redirect_to root_path unless user_signed_in? && current_user.admin?
    @seller = User.new
  end

  def create
    @seller = User.new(seller_params)
    @seller.password = 'default'
    @seller.password_confirmation = 'default'
    @seller.role = :seller
    if user_signed_in? && current_user.admin? && @seller.save
      flash[:success] = 'Vendedor criado com sucesso!'
    else
      flash[:danger] = 'Vendedor não pode ser criado'
    end
    redirect_to root_path
  end

  private

  def seller_params
    params.require(:user).permit(:name, :nickname, :email)
  end
end
