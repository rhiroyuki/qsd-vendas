class OrdersController < ApplicationController
  API_URL = Rails.configuration.api['products_api'].freeze
  before_action :set_menus, only: [:show, :checkout, :resume]
  before_action :authenticate_user!, except: [:create, :checkout]
  before_action :authenticate_user_to_order!, except: [:create, :checkout]
  before_action :authenticate_user_to_checkout, only: [:checkout]
  before_action :set_order_attributes, only: [:resume, :show]
  before_action :redirect_admin, except: [:show, :cancel]

  def create
    if valid_params?
      @order = Order.new(order_params)
      define_user_and_seller
      @order.save

      check_redirect_path

    else
      flash[:danger] = 'Pedido não pode ser gerado'
      redirect_to root_path
    end
  end

  def coupon
    coupon = Coupon.apply_coupon(params[:coupon], @order)

    if coupon
      @order.apply_coupon coupon
      flash[:success] = 'Cupom aplicado com sucesso'
    else
      flash[:danger] = 'Cupom inválido'
    end

    redirect_to checkout_order_path @order
  end

  def show; end

  def cancel
    @order.canceled!
    flash[:success] = 'Pedido cancelado com sucesso'
    redirect_to @order
  end

  def finish
    @order.closed!
    flash[:success] = 'Pedido finalizado com sucesso'
    redirect_to @order
  end

  def resume
    redirect_to @order unless @order.opened?
  end

  def checkout
    check_integrity_order
    @credit = Credit.new
    @payment = Payment.new
  end

  private

  def define_user_and_seller
    current_user.orders << @order if user_signed_in? && current_user.customer?
    @order.seller = current_user if user_signed_in? && current_user.seller?
  end

  def check_redirect_path
    if user_signed_in? && current_user.seller?
      redirect_to seller_checkout_path @order
    else
      redirect_to checkout_order_path @order
    end
  end

  def set_order_attributes
    @order = Order.find params[:id]
    @product = Product.find @order.product
    @plan = Plan.find @order.plan
  end

  def valid_params?
    return false unless params.key?(:price_id)
    response = RestClient.get("#{API_URL}/prices/#{params[:price_id]}")
    return false if response.code != 200

    price = JSON.parse(response.body)

    price['price']['product_id'] == params[:product].to_i &&
      price['price']['plan_id'] == params[:plan].to_i &&
      price['price']['value'] == params[:price]
  end

  def order_params
    { product: params[:product], plan: params[:plan], price: params[:price],
      period: params[:period], source_ip: request.remote_ip, status: 'opened' }
  end

  def authenticate_user_to_order!
    @order = Order.find params[:id]
    return if @order.valid_user?(current_user) || current_user.admin?
    flash[:danger] = 'Não foi possível concluir a operação.'
    redirect_to root_path
  end

  def authenticate_user_to_checkout
    set_order_attributes
    return if user_signed_in?
    flash[:info] = 'Para continuar o checkout do pedido' \
      "#{@order.id} faça login ou registre-se"
    redirect_to new_user_session_path
  end

  def check_integrity_order
    return if @order.check_integrity?(request.remote_ip, current_user)
    flash[:danger] = 'Não foi possível concluir a operação.'
    redirect_to root_path
  end
end
