module Seller
  class OrdersController < ApplicationController
    before_action :set_menus, except: [:send_order]
    before_action :authenticate_user!

    def checkout
      @order = Order.find params[:id]

      redirect_to root_path unless @order.seller == current_user
      @product = Product.find @order.product
      @plan = Plan.find @order.plan
      @customers = User.customer
    end

    def send_order
      order = Order.find params[:id]
      order.user = User.find params[:order][:user_id]
      order.save
      OrdersMailer.send_order(order, order.user).deliver_now
      flash[:succes] = 'Pedido gerado com sucesso. ' \
                        "Email enviado para: #{order.user.email}"
      redirect_to root_path
    end
  end
end
