module Customer
  class OrdersController < ApplicationController
    before_action :require_customer
    before_action :set_menus, only: [:index]

    def index
      @statuses = Order.statuses_attributes
      @orders = current_user.orders.all
      @current_filter = params[:filter]
      @orders = @orders.where(status: @current_filter) if @current_filter
      flash.now[:info] = 'Não foram encontrados pedidos' if @orders.empty?
    end

    private

    def require_customer
      redirect_to root_path unless current_user.try(:customer?)
    end
  end
end
