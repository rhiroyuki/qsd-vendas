class OrdersMailer < ApplicationMailer
  def send_order(order, customer)
    @customer = customer
    @order = order
    mail(to: @customer.email, subject: 'Conclua sua compra')
  end
end
