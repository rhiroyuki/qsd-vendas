Rails.application.routes.draw do
  devise_for :users
  root 'cart#products'

  namespace :admin do
    resources :orders, only: [:index]
  end

  namespace :customer do
    resources :orders, only: [:index]
  end

  resources :orders, only: [:show, :create] do
    member do
      put 'cancel'
      put 'finish'
      get 'resume'
      get 'checkout'
      put 'coupon'
    end
  end

  #namespace :cart do
  resource :cart, only: [] do
      get 'products', to: 'cart#products'
      get ':product/:product_id/plans', to: 'cart#plans', as: 'product_plans'
      get ':product/:product_id/:plan/:plan_id/prices', to: 'cart#prices', as: 'plan_prices'
  end

  resources :payments, only: [:create]
  resources :sellers, only: [:new, :create]

  namespace :seller do
    get '/:id/checkout', to: 'orders#checkout', as: 'checkout'
    post 'send/order/:id', to: 'orders#send_order', as: 'send_order'
  end

  resources :payments, only: [:create]
end
