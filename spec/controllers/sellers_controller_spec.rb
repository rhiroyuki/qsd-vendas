require 'rails_helper'
RSpec.describe SellersController do
  describe 'POST create' do
    it 'try to create seller as customer' do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      user = create(:user)
      seller = build(:user, :seller)
      sign_in user
      get :create, params: { user: { name: seller.name, email: seller.email,
                                     nickname: seller.nickname } }

      expect(flash[:danger]).to eq 'Vendedor não pode ser criado'
    end
    it 'try to create seller as seller' do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      user = create(:user, :seller)
      seller = build(:user, :seller)
      sign_in user
      get :create, params: { user: { name: seller.name, email: seller.email,
                                     nickname: seller.nickname } }

      expect(flash[:danger]).to eq 'Vendedor não pode ser criado'
    end
    it 'try to create seller as visitor' do
      seller = build(:user, :seller)
      get :create, params: { user: { name: seller.name, email: seller.email,
                                     nickname: seller.nickname } }

      expect(flash[:danger]).to eq 'Vendedor não pode ser criado'
    end
  end
end
