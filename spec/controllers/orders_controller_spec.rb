require 'rails_helper'
RSpec.describe OrdersController do
  describe 'POST create' do
    it 'try to create order with invalid params' do
      @request.env['devise.mapping'] = Devise.mappings[:user]
      user = create(:user)
      sign_in user
      get :create

      expect(flash[:danger]).to eq 'Pedido não pode ser gerado'
    end
  end
end
