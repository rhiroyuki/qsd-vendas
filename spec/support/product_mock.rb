def product_mock
  stub_request(:get, 'http://vps1549.publiccloud.com.br/api/v1/products/1')
    .to_return(status: 200, body: '{"product":{"id":1,"name":"EMAIL",'\
    '"description":"SERVIÇO DE EMAIL","icon":{"url":null,"thumb":{"url":'\
    'null},"medium":{"url":null}},"product_key":"MAIL12"}}', headers: {})
end

def plan_mock
  stub_request(:get, 'http://vps1549.publiccloud.com.br/api/v1/plans/1')
    .to_return(status: 200, body: '{"plan":{"id":1,"name":"EMAIL BASIC'\
    '","description":"EMAIL BASICO","details":"10GB de armazenamento",'\
    '"status":"active","plan_option":[{"price_id":1,"periodicity":'\
    '"MENSAL","price":"10.0"},{"price_id":2,"periodicity":"TRIMENSAL"'\
    ',"price":"20.0"},{"price_id":3,"periodicity":"SEMESTRAL","price"'\
    ':"30.0"},{"price_id":4,"periodicity":"ANUAL","price":"40.0"}]}}',
               headers: {})
end

def products_mock
  stub_request(:get, 'http://vps1549.publiccloud.com.br/api/v1/products')
    .to_return(status: 200, body: '{"products":[{"id":1,"name":"EMAIL",'\
    '"description":"SERVIÇO DE EMAIL","icon":{"url":null,"thumb":{"url":null},'\
    '"medium":{"url":null}},"product_key":"MAIL12"},{"id":2,"name":"HOPSEDAGEM'\
    '","description":"HOSPEDAGEM DE SITE","icon":{"url":null,"thumb":{"url":'\
    'null},"medium":{"url":null}},"product_key":"HOSP01"}]}', headers: {})
end

def product_plans_mock
  stub_request(:get, 'http://vps1549.publiccloud.com.br/api/v1/products/1/'\
  'plans').to_return(status: 200, body: '{"plans":[{"id":1,"name":'\
  '"EMAIL BASIC","description":"EMAIL BASICO","details":"10GB de '\
  'armazenamento","status":"active","plan_option":[{"price_id":1,'\
  '"periodicity":"MENSAL","price":"10.0"},{"price_id":2,"periodicity":'\
  '"TRIMENSAL","price":"20.0"},{"price_id":3,"periodicity":"SEMESTRAL",'\
  '"price":"30.0"},{"price_id":4,"periodicity":"ANUAL","price":'\
  '"40.0"}]}]}', headers: {})
end

def periodicities_mock
  stub_request(:get, 'http://vps1549.publiccloud.com.br/api/v1/plans/1')
    .to_return(status: 200, body: '{"plan":{"id":1,"name":"EMAIL BASIC",'\
    '"description":"EMAIL BASICO","details":"10GB de armazenamento","status":'\
    '"active","plan_option":[{"price_id":1,"periodicity":"MENSAL",'\
    '"price":"10.0"},{"price_id":2,"periodicity":"TRIMENSAL","price":'\
    '"20.0"},{"price_id":3,"periodicity":"SEMESTRAL","price":"30.0"},'\
    '{"price_id":4,"periodicity":"ANUAL","price":"40.0"}]}}', headers: {})
end

def price_mock
  stub_request(:get, 'http://vps1549.publiccloud.com.br/api/v1/prices/1')
    .to_return(status: 200,
               body: '{"price":{"id":1,"value":"10.0","plan_id":1,'\
    '"product_id":1,"periodicity":{"id":1,"name":"MENSAL","months":1}}}',
               headers: {})
end

def set_mocks
  product_plans_mock
  product_mock
  products_mock
  plan_mock
  periodicities_mock
  price_mock
end
