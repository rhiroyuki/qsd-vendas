require 'rails_helper'

RSpec.describe Coupon do
  context '#apply_coupon' do
    class FakeResponse
      attr_reader :body, :code
      def initialize(params)
        @body = params[:body]
        @code = params[:code]
      end
    end
    it 'returns coupon successfully' do
      coupon = Coupon.new(key: 1, type: 'percent', value: 10.0)
      response = FakeResponse.new(body: '{ "discount": "10.0" }', code: 200)
      allow(RestClient).to receive(:patch).and_return(response)
      expect(Coupon.apply_coupon(1, create(:order)).key).to eq(coupon.key)
    end
    it 'returns nil' do
      response = FakeResponse.new(body: nil, code: 412)
      allow(RestClient).to receive(:patch).and_return(response)
      expect(Coupon.apply_coupon(1, create(:order))).to be_nil
    end
  end
end
