FactoryGirl.define do
  factory :user do
    name                  { Faker::Name.name }
    email                 { Faker::Internet.email(name.split.join('.')) }
    password              { Faker::Internet.password }
    password_confirmation { password }
    role                  :customer
    nickname              { Faker::Internet.user_name }

    trait :admin do
      role :admin
    end

    trait :seller do
      role :seller
    end
    trait :admin do
      role :admin
    end
  end
end
