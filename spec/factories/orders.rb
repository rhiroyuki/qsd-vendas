FactoryGirl.define do
  factory :order do
    status 'opened'
    product 1
    plan 1
    price 10.00
    period 'Mensal'
    coupon 'hosP_3xKy72'
    source_ip '0.0.0.0'
    payment nil
    user nil
  end
end
