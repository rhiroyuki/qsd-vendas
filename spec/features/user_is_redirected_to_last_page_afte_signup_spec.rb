require 'rails_helper'

feature 'User is redirected to last page after sign up' do
  before do
    products_mock
    product_plans_mock
    product_mock
    plan_mock
  end
  scenario 'successfully' do
    user = build(:user)
    product = Product.all.first
    plan = product.plans.first

    visit plan_prices_cart_path product.name, product.id, plan.name, plan.id

    within('section#navbar') { click_on 'Cadastrar' }

    within('section#user-form') do
      fill_in 'Nome',                 with: user.name
      fill_in 'E-mail',               with: user.email
      fill_in 'Senha',                with: user.password, id: 'user_password'
      fill_in 'Confirmação de Senha', with: user.password_confirmation

      click_on 'Criar Conta'
    end

    expect(current_path).to eq plan_prices_cart_path(product.name,
                                                     product.id,
                                                     plan.name, plan.id)
  end
end
