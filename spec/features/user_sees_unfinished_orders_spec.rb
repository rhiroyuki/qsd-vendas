require 'rails_helper'

feature 'User sees unfinished orders' do
  before do
    set_mocks
  end
  scenario 'successfully' do
    user = create(:user)
    login_as user, scope: :user
    order = create(:order, user: user)

    visit root_path

    within('section#left-bar') do
      click_on 'Meus Pedidos'
    end

    expect(page).to have_content order.product
    expect(page).to have_content order.plan
    expect(page).to have_content order.period
  end

  scenario 'and proceed to conclude it' do
    user = create(:user)
    login_as user, scope: :user
    order = create(:order, user: user)

    visit customer_orders_path

    click_on 'Retomar'
    expect(current_path).to eq checkout_order_path(order)
  end

  scenario 'and proceed to cancel it' do
    user = create(:user)
    login_as user, scope: :user
    order = create(:order, user: user)

    visit customer_orders_path

    click_on 'Cancelar'
    expect(page).to have_content 'Cancelado'
    expect(current_path).to eq order_path(order)
  end
end
