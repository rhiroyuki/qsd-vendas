require 'rails_helper'

feature 'Seller create order for customer' do
  scenario 'Successfuly' do
    set_mocks
    seller = create(:user, :seller)
    custumer = create(:user)

    login_as seller, scope: :user

    visit root_path

    within('section#products') { first('a.ls-btn-primary').click }

    within('section#plans') { first('a.ls-btn-primary').click }

    within('section#prices') { first('input.ls-btn-primary').click }

    within 'section#customers' do
      select custumer.name, from: 'Clientes'
      click_on 'Gerar Pedido'
    end

    within 'section#flash-messages' do
      expect(page).to have_content 'Pedido gerado com sucesso.' \
                                   " Email enviado para: #{custumer.email}"
    end
  end
end
