require 'rails_helper'

feature 'user creates seller' do
  before do
    set_mocks
  end
  scenario 'as admin successfully' do
    admin = create(:user, :admin)
    seller = build(:user, :seller)
    login_as admin, scope: :user

    visit root_path
    click_on 'Cadastrar Vendedor'

    within('section#user-form') do
      fill_in 'Nome',                 with: seller.name
      fill_in 'E-mail',               with: seller.email
      fill_in 'Apelido',              with: seller.nickname

      click_on 'Criar Vendedor'
    end

    within 'section#flash-messages' do
      expect(page).to have_content('Vendedor criado com sucesso!')
    end
  end
  scenario 'as visitor unsuccessfully' do
    visit new_seller_path

    expect(current_path).to eq(root_path)
  end
  scenario 'as customer unsuccessfully' do
    user = create(:user)
    login_as user, scope: :user

    visit new_seller_path

    expect(current_path).to eq(root_path)
  end
  scenario 'as seller unsuccessfully' do
    seller = create(:user, :seller)
    login_as seller, scope: :user

    visit new_seller_path

    expect(current_path).to eq(root_path)
  end
end
