require 'rails_helper'

feature 'user view plan options' do
  before do
    set_mocks
  end
  def get_plans(product_id)
    api = Rails.configuration.api['products_api']

    response = RestClient.get("#{api}/products/#{product_id}/plans")
    plans = JSON.parse(response.body)

    plans['plans'].map do |plan|
      Plan.new plan
    end
  end

  scenario 'successfully' do
    product_id = 1

    plans = get_plans product_id

    visit product_plans_cart_path 'Product_Name', product_id

    within('section.product') do
      plans.each do |plan|
        expect(page).to have_link plan.name
        expect(page).to have_content plan.description
        expect(page).to have_content plan.details
      end
    end
  end
end
