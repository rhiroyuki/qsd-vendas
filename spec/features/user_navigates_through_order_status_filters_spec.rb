require 'rails_helper'

feature 'User navigates through order status filters' do
  before do
    user = create(:user)
    login_as user, scope: :user
    @order_opened = create(:order, user: user, status: 'opened')
    @order_closed = create(:order, user: user, status: 'closed',
                                   period: 'Semestral')
    @order_canceled = create(:order, user: user, status: 'canceled',
                                     period: 'Anual')
  end

  scenario 'and see all orders' do
    visit customer_orders_path

    click_on 'Todos'

    expect(page).to have_content @order_opened.period
    expect(page).to have_content @order_closed.period
    expect(page).to have_content @order_canceled.period
  end

  scenario 'and see only opened orders' do
    visit customer_orders_path

    click_on 'Aberto'

    expect(page).to have_content @order_opened.period
    expect(page).not_to have_content @order_closed.period
    expect(page).not_to have_content @order_canceled.period
  end

  scenario 'and see only closed orders' do
    visit customer_orders_path

    click_on 'Finalizado'

    expect(page).not_to have_content @order_opened.period
    expect(page).to have_content @order_closed.period
    expect(page).not_to have_content @order_canceled.period
  end

  scenario 'and see only canceled orders' do
    visit customer_orders_path

    click_on 'Cancelado'

    expect(page).not_to have_content @order_opened.period
    expect(page).not_to have_content @order_closed.period
    expect(page).to have_content @order_canceled.period
  end
end
