require 'rails_helper'

feature 'Visitor sees products/plans prices' do
  before do
    set_mocks
  end
  scenario 'successfully' do
    product = Product.all.first

    plan = Plan.find(1)

    periodicities_list = plan.periodicities

    visit plan_prices_cart_path(product.name, product.id, plan.name, plan.id)

    periodicities_list.each do |periodicity|
      expect(page).to have_link periodicity.name.capitalize
      expect(page).to have_content periodicity.price
    end
  end
end
