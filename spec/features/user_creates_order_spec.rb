require 'rails_helper'

feature 'User creates order' do
  before do
    products_mock
    product_mock
    plan_mock
    product_plans_mock
    periodicities_mock
    price_mock
  end
  scenario 'when not logged in' do
    user = create(:user)
    product = Product.find 1
    plan = product.plans.first
    periodicity = plan.periodicities.first

    visit plan_prices_cart_path product.name, product.id, plan.name, plan.id

    within 'div#Mensal' do
      click_on 'Contratar'
    end

    within('section#sign-in-form') do
      fill_in 'E-mail', with: user.email
      fill_in 'Senha',  with: user.password

      click_on 'Entrar'
    end

    expect(page).to have_content product.name
    expect(page).to have_content plan.name
    expect(page).to have_content periodicity.name
    expect(page).to have_content periodicity.price
  end

  scenario 'when logged in' do
    user = create(:user)
    product = Product.find 1
    plan = product.plans.first
    periodicity = plan.periodicities.first

    visit new_user_session_path

    within('section#sign-in-form') do
      fill_in 'E-mail', with: user.email
      fill_in 'Senha',  with: user.password

      click_on 'Entrar'
    end

    visit plan_prices_cart_path product.name, product.id, plan.name, plan.id

    within 'div#Mensal' do
      click_on 'Contratar'
    end

    expect(page).to have_content product.name
    expect(page).to have_content plan.name
    expect(page).to have_content periodicity.name
    expect(page).to have_content periodicity.price
  end

  scenario 'check user IP' do
    user = create(:user)
    product = Product.find 1
    plan = product.plans.first
    periodicity = plan.periodicities.first
    order = create(:order, product: product.id,
                           plan: plan.id,
                           period: periodicity.name,
                           price: periodicity.price,
                           source_ip: '175.100.10.1')
    allow_any_instance_of(ActionDispatch::Request)
      .to receive(:ip).and_return('175.100.10.2')

    visit checkout_order_path(order)

    within('section#sign-in-form') do
      fill_in 'E-mail', with: user.email
      fill_in 'Senha',  with: user.password

      click_on 'Entrar'
    end

    within('#flash-messages') do
      expect(page).to have_content 'Não foi possível concluir a operação.'
    end
  end
end
