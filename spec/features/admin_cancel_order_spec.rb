require 'rails_helper'

feature 'admin cancel order' do
  scenario 'successfully' do
    product_mock
    plan_mock

    admin = create(:user, role: 'admin')
    user = create(:user, name: 'Usuário')

    login_as admin, scope: :user
    order_opened = create(:order, user: user, status: 'opened')
    order_closed = create(:order, user: user, status: 'closed',
                                  period: 'Semestral')
    order_canceled = create(:order, user: user, status: 'canceled',
                                    period: 'Anual')

    visit admin_orders_path
    within("section#order#{order_opened.id}") do
      click_on 'Cancelar'
    end
    expect(page).not_to have_content order_closed.period
    expect(page).not_to have_content order_canceled.period
    expect(page).to have_content order_opened.period
    expect(page).to have_content 'Cancelado'
    expect(current_path).to eq order_path(order_opened)
  end
end
