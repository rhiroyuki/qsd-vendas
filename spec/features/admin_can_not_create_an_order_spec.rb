require 'rails_helper'

feature 'Admin can not create an order' do
  scenario 'Successfuly' do
    set_mocks
    admin = create(:user, :admin)
    login_as admin, scope: :user

    visit root_path

    expect(page).to have_current_path admin_orders_path
  end
end
