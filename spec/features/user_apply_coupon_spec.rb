require 'rails_helper'

feature 'User apply coupon' do
  before do
    set_mocks
    @user = create(:user)
    login_as @user, scope: :user
  end

  scenario 'apply invalid coupon' do
    allow(Coupon).to receive(:apply_coupon).and_return(nil)
    order = create(:order, coupon: nil, user: @user)

    visit checkout_order_path(order)

    fill_in 'Insira seu cupom', with: 'CupomInvalido'
    click_on 'Aplicar Cupom'

    within('strong#price') do
      expect(page).to have_content order.price
    end
    expect(page).to have_content 'Cupom inválido'
    expect(page).to have_css 'form#coupon_form'
  end

  scenario 'apply relative coupon successfully' do
    coupon = Coupon.new key: 'cpr001', type: 'percent', value: 10

    allow(Coupon).to receive(:apply_coupon).and_return(coupon)
    order = create(:order, coupon: nil, user: @user)

    visit checkout_order_path(order)

    new_value = order.price * (1 - (coupon.value / 100.0))

    fill_in 'Insira seu cupom', with: coupon.key
    click_on 'Aplicar Cupom'

    within('strong#price') do
      expect(page).to have_content new_value
    end
    expect(page).to have_content 'Cupom aplicado com sucesso'
    expect(page).not_to have_css 'form#coupon_form'
  end
end
