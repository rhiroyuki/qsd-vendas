require 'rails_helper'

feature 'Visitor choices products' do
  before do
    set_mocks
  end
  scenario 'Sees all products' do
    products = Product.all

    visit root_path

    products.each do |product|
      expect(page).to have_content product.name
      expect(page).to have_content product.description
    end
  end
end
