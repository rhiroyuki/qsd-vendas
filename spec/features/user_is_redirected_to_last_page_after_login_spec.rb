require 'rails_helper'

feature 'User is redirected to last page after login' do
  before do
    set_mocks
  end
  scenario 'successfully' do
    user = create(:user)
    product = Product.all.first
    plan = product.plans.first
    visit plan_prices_cart_path product.name, product.id, plan.name, plan.id

    within('section#navbar') { click_on 'Entrar' }

    within('section#sign-in-form') do
      fill_in 'E-mail', with: user.email
      fill_in 'Senha',  with: user.password

      click_on 'Entrar'
    end

    expect(current_path).to eq plan_prices_cart_path(product.name, product.id,
                                                     plan.name, plan.id)
  end
end
