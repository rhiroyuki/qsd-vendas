# Sistema de Vendas QSD

## Sobre  
<br>
A aplicação cobre todo o processo de venda de produtos para os clientes.  

O processo básico de venda passa pelas etapas:  
<br>
1. Escolha do produto
2. Escolha do plano do produto
3. Escolha da periodicidade e preço referente ao plano escolhido
4. Identificação do cliente (login ou novo cadastro)
5. Definição da forma de pagamento
  * Aplicar cupom de desconto (opcional)
  * Indicar vendedor (opcional)
6. Finalizar ou cancelar o pedido  

<br>

Todos os dados referentes a produtos e cupons são consumidos de APIs
responsáveis por gerenciar tais itens.

A aplicação permite que um cliente visualize todo os seus pedidos (abertos,
finalizados e cancelados), e dê continuidade aos pedidos abertos.

Usuários administradores podem cadastrar vendedores ao sistema. Podem também
visualizar todos os pedidos e, se necessário, cancelar pedidos em aberto.

Vendedores podem abrir um pedido para que um cliente apenas concluam a compra.

<br>
<br>
## Configurações do Sistema
---
Para o correto funcionamento do sistema é preciso assegurar que as informações
contidas no arquivo `config/apis_url.yml`, a respeito dos endereços das APIs ,
estejam corretas.  
<br>
O arquivo `db/seeds.rb` cria um usuário padrão para administrador do sistema. O
arquivo é executado no setup da aplicação, portanto é importante antes de
executar o comando `bin/setup` (durante o processo de instalação) assegurar que
os dados utilizados para a criação do usuário atendem as necessidades. Por
padrão o nome do usuário criado é **admin**, email **admin@admin.com** e senha
**123456**  
<br>
Os usuários cadastrados como vendedores tem senha padrão **default**
<br>
<br>
## Instalação
---
### Dependências

A aplicação foi desenvolvida em um ambiente rodando **Ruby 2.3.3, Rails 5.0.1**
e banco de dados **Postgres 9.6**
<br>
<br>
### Utilizando via Docker
<br>
Clone o projeto da aplicação:
```
git clone http://qsd.campuscode.com.br/projeto_final/vendas.git
```
<br>

Faça a instalação do Docker e do Docker Compose:  
[Docker Install](https://docs.docker.com/engine/installation/)  
[Docker Compose Install](https://docs.docker.com/compose/install/)  
<br>
Faça o build das imagens para os containers:
```
docker-compose build
```
<br>
Abra um terminal no container da aplicação Rails:
```
docker-compose run --rm web bash
```
<br>
Dentro do terminal do container rode o comando para realizar o setup da
aplicação:
```
bin/setup
```
<br>
Saia do terminal do container com o comando `exit`, e rode o container da
aplicação:
```
docker-compose up web
```
<br>
#### Configurações para o  Docker  
Por padrão o container foi configurado para espelhar a **porta 3000** (porta
onde roda a aplicação Rails) na **porta 80** da máquina host.  
Para alterar basta editar o arquivo **docker-compose.yml** a configuração
**ports** `- "80:3000"` trocando o valor **80** para a porta desejada.  
<br>
Outro padrão do sistema é iniciar a aplicação no `ENVIRONMENT=development`
quando rodado o comando `docker-compose up web`.  
Para alterar é necessário editar o arquivo **docker-compose.yml** a configuração
**command** `rails s -b 0.0.0.0`, alterando o comando para algo como `rails s -b
0.0.0.0 -e production`.  
<br>
**Obs:** É importante notar que alterando o **ENVIRONMENT** para production é
necessário setar uma variável de ambiente para `secret_key` do rails. Veja o
arquivo `config/secrets.yml`.

<br>
<br>
### Utilizando sem Docker
<br>

É necessário realizar as instalações do Ruby, do Rails e do Postgres:  
[Ruby](https://www.ruby-lang.org/pt/documentation/installation/)  
[Rails](http://guides.rubyonrails.org/getting_started.html#installing-rails)  
[Postgres](https://wiki.postgresql.org/wiki/Detailed_installation_guides)  
<br>
Clone o projeto da aplicação:
```
git clone http://qsd.campuscode.com.br/projeto_final/vendas.git
```
<br>
O primeiro passo após clonar a aplicação é ajustar as configurações do arquivo
`config/database.yml` para corresponder com a sua instalação do Postgres  
<br>
<br>
Após ajustadas as configurações do banco de dados acesse a raiz do projeto e
rode o comando para realizar o setup da aplicação:
```
bin/setup
```
<br>
Para levantar a aplicação execute o comando:
```
rails s -b 0.0.0.0
```
<br>
Para subir a aplicação no `ENVIRONMENT` de produção execute o comando acima
seguido de `-e production`.  
**Obs:** É importante notar que alterando o **ENVIRONMENT** para production é
necessário setar uma variável de ambiente para `secret_key` do rails. Veja o
arquivo `config/secrets.yml`.  
<br>
<br>
# Informações para Desenvolvimento
<br>
## Layouts  
<br>
 Foi utilizado como tema da aplicação o Locastyle.  
 <br>
#### Guias de Layout
Para exibir mensagens utilizar `flash[:classes_alerts_locastyle]`
* flash[:success] = 'Ação feita com sucesso'
* flash[:danger] = 'Ocorreu um erro ao executar a ação'
* flash[:info] = 'Apenas uma informação qualquer'  

<br>
Para atribuir Títulos ao Header e a Página utilizar helper `content_for` nas
views, para os `yelds` **header_title** e **page_title**  
```ruby
content_for :header_title do
  Title to Header
end

content_for :page_title do
  Title to page
end
```
<br>
Os menus de Usuário e o menu Lateral da página são carregados via **partials**,
para identificar qual partial carregar o layout espera dois atributos
`@left_menu` e `@top_menu`, esses atributos são setados pela **action**
`set_menus`. Esta action verifica a `role` do usuário logado para determinar o
escopo de menu que irá carregar. Ou seja para o usuário com `role= :admin`, o
sistema espera que existam as partials **left_admin_menu** e **top_admin_menu**
dentro da pasta `app/views/application`.
```
```
```
```
```
```
```
```
```
```
```
```
```
```
```
```
